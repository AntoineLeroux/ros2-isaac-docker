# Filename: <Makefile>
# Copyright (C) <2022> Authors: <Antoine Leroux>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

path = docker

clean:
	docker system prune -a -f

run:
	xhost +local:docker && ./run.sh

stop_amd64:
	docker stop ros2_isaac-gpu-x86_64-container

rm_amd64:
	docker rm ros2_isaac-gpu-x86_64-container

exec_amd64:
	xhost +local:docker && docker exec -it ros2_isaac-gpu-x86_64-container bash

attach_amd64:
	xhost +local:docker && docker attach ros2_isaac-gpu-x86_64-container

stop_arm64:
	docker stop ros2_isaac-gpu-aarch64-container

rm_arm64:
	docker rm ros2_isaac-gpu-aarch64-container

exec_arm64:
	xhost +local:docker && docker exec -it ros2_isaac-gpu-aarch64-container bash

attach_arm64:
	xhost +local:docker && docker attach ros2_isaac-gpu-aarch64-container