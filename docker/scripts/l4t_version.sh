#!/bin/bash

# Filename: <l4t_version.sh>
# Copyright (C) <2022> Authors: <Antoine Leroux>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

L4T_VERSION_STRING=$(head -n 1 /etc/nv_tegra_release)

if [ -z "$L4T_VERSION_STRING" ]; then
        echo "reading L4T version from \"dpkg-query --show nvidia-l4t-core\""

        L4T_VERSION_STRING=$(dpkg-query --showformat='${Version}' --show nvidia-l4t-core)
        L4T_VERSION_ARRAY=(${L4T_VERSION_STRING//./ })

        L4T_RELEASE=${L4T_VERSION_ARRAY[0]}
        L4T_REVISION=${L4T_VERSION_ARRAY[1]}
else
        echo "reading L4T version from /etc/nv_tegra_release"

        L4T_RELEASE=$(echo $L4T_VERSION_STRING | cut -f 2 -d ' ' | grep -Po '(?<=R)[^;]+')
        L4T_REVISION=$(echo $L4T_VERSION_STRING | cut -f 2 -d ',' | grep -Po '(?<=REVISION: )[^;]+')
fi

L4T_REVISION_MAJOR=${L4T_REVISION:0:1}
L4T_REVISION_MINOR=${L4T_REVISION:2:1}

L4T_VERSION="$L4T_RELEASE.$L4T_REVISION"

echo "L4T BSP Version:  L4T R$L4T_VERSION"