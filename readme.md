# Introduction

Repository made for running ROS2 with ISAAC, OpenCV CUDA, Aravis support on both x86_64 and aarch64 architectures in a docker container.

Native support for SVS-VISTEK USB Cameras. See Setup for instruction for other cameras...

- amd64/x86_64 architecture : Tested & Working container
- aarch64 : Not tested yet but should work

# Setup

## Prerequisites

You need to have a correctly installed docker system with nvidia-docker2 package installed and configured. Please see [NVIDIA-DOCKER2 Setup](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#getting-started) for more details...

## Udev rules for Aravis Cameras

Create `70-aravis.rules` in `/etc/udev/rules.d` folder and insert the following.

```bash
SUBSYSTEM=="usb", ATTRS{idVendor}=="2c17", KERNEL=="ttyUSB[0-9]*", MODE:="0666", TAG+="uaccess", TAG+="udev-acl", SYMLINK+="svs_vistek"
```

Enter the correspond idVendor according to your `lsusb` output.

By default the SYMLINK argument is set to `svs_vistek`. If you want to change this, you need to edit `run.sh` on `-v "/dev/svs_vistek:/dev/svs_vistek"`.

# Launch

## Run container

Run `make run` at repository root.

The folder at project root `some_ws` is bind to `/workspaces/` in container so that you can edit your code as you wish from the host.

To open a bash terminal inside the container : `make exec_amd64` or `make exec_arm64` according to your architecture.

## X11 Forward for GL Apps

You must run `xhost +local:docker` in the host terminal before connecting to the container...

X11 apps must be run with sudo if you want them to actually work.
