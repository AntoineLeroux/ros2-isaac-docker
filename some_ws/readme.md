# Volume binding

- Place here your code/work.

- You can also customize the init script and edit `run.sh` at root and add/edit volume mounting :

    ```bash
    --mount type=bind,source=$ROOT/some_ws,target=/workspaces/some_ws
    ```
